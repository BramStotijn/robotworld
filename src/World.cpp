#include "World.hpp"
#include "RobotWorld.hpp"
#include <algorithm>

World::World(/* args */) : driven(std::make_shared<Model::NormalLine>("RED")), robotPos(wxDefaultPosition), robotOrientation(0)
{
}

World::~World()
{
}


void World::setPosRobot(const Point& pos)
{
    std::lock_guard<std::mutex> lock(getLinesLock);
    driven->addPoint(pos);
    robotPos = pos;
}

void World::setOrientation(double orientation)
{
    robotOrientation = orientation;
}

World& World::getWorld()
{
    static World world;
    return world;
}

const Point& World::getPosRobot() const
{
    return robotPos;
}


double World::getOrientation() const
{
    return robotOrientation;
}

const std::vector<Model::WallPtr>& World::getWalls() const
{
    return Model::RobotWorld::getRobotWorld().getWalls();
}

std::vector<Model::LinePtr> World::getLines()
{
    std::lock_guard<std::mutex> lock(getLinesLock);
    std::vector<Model::LinePtr> lines {driven};
    return lines;
}

void World::attachActuator(std::shared_ptr<Actuator> actuator)
{
    actuators.push_back(actuator);
}

void World::attachSensor(std::shared_ptr<Sensor> sensor)
{
    sensors.push_back(sensor);
}


struct sensorTypeCompare
{
    SensorType sensor;
    sensorTypeCompare(SensorType searchFor) : sensor(searchFor)
    {
    }

    bool operator()(const std::shared_ptr<Sensor>& base)
    {
        return (base->getType() == sensor);
    }
};


std::shared_ptr<Sensor> World::getSensor(SensorType sensor) const
{
    auto measureDevice = std::find_if(sensors.begin(), sensors.end(), sensorTypeCompare(sensor));
    if(measureDevice != sensors.end())
    {
        return (*measureDevice);
    }
    throw(std::invalid_argument("invalid sensor"));
}

std::shared_ptr<Measurement> World::measureSensor(SensorType sensor)
{
    auto measureDevice = std::find_if(sensors.begin(), sensors.end(), sensorTypeCompare(sensor));
    if(measureDevice != sensors.end())
    {
        return (*measureDevice)->measure(*this);
    }
    throw(std::invalid_argument("invalid sensor"));
}

struct actuatorTypeCompare
{
    ActuatorType actuator;
    actuatorTypeCompare(ActuatorType searchFor) : actuator(searchFor){}
    bool operator()(const std::shared_ptr<Actuator>& base)
    {
        return (base->getType() == actuator);
    }
};

std::shared_ptr<Actuator> World::getActuator(ActuatorType actuator)
{
    auto actuatorDevice = std::find_if(actuators.begin(), actuators.end(), actuatorTypeCompare(actuator));
    if(actuatorDevice != actuators.end())
    {
        return (*actuatorDevice);
    }
    throw(std::invalid_argument("invalid actuator"));
}

void World::actuate(ActuatorType actuator, std::shared_ptr<BaseCommand> command)
{
    auto actuatorDevice = std::find_if(actuators.begin(), actuators.end(), actuatorTypeCompare(actuator));
    if(actuatorDevice != actuators.end())
    {
        (*actuatorDevice)->handleCommand(command);
    } else {
        throw(std::invalid_argument("invalid actuator"));
    }
}

void World::initSensors()
{
    for(auto& sensor : sensors)
    {
        sensor->init();
    }
}

void World::clearLines()
{
    driven->clear();
}


