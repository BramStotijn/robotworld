#ifndef ROBOTWORLDWOR_PARTICLE_HPP
#define ROBOTWORLDWOR_PARTICLE_HPP

#include "Point.hpp"
struct Particle
{
    double weight;
    Point position;
    double orientation;
    Particle() : weight(0), position(Point(0,0)), orientation(0) {}
    Particle(double aWeight, const Point& aPosition) : weight(aWeight), position(aPosition) {}
    virtual ~Particle(){}
};

#endif //ROBOTWORLDWOR_PARTICLE_HPP
