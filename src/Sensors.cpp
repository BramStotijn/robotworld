#include "Sensors.hpp"
#include "World.hpp"
#include "Shape2DUtils.hpp"
#include "Widgets.hpp"
#include "ParticleFilter.hpp"
#include "Robot.hpp"

LidarMeasurement::LidarMeasurement()
{
}

LidarMeasurement::~LidarMeasurement()
{
}

double LidarMeasurement::ParticleWeight(const Particle& particle) const
{
    std::shared_ptr<LidarSensor> sensor = std::dynamic_pointer_cast<LidarSensor>(World::getWorld().getSensor(SensorType::LIDARSENSOR));
    if(sensor != nullptr)
    {
        bool smallDiffrence = false;
        bool largeDiffrence = false;
        std::array<double, 180> particleDistances = sensor->wallDistance(particle.position, World::getWorld().getWalls());
        for(auto i = 0; i < 180; ++i)
        {
            if(particleDistances[i] - distances[i] < 25 && particleDistances[i] - distances[i] > -25)
            {

            } else if(particleDistances[i] - distances[i] < 50 && particleDistances[i] - distances[i] > -50)
            {
                smallDiffrence = true;
            } else {
                largeDiffrence = true;
            }
        }
        if(largeDiffrence)
        {
            return 0;
        } else if(smallDiffrence)
        {
            return 1.5;
        }
        return 3;
    }
    return 0;
}

LidarSensor::LidarSensor(double noiseMean, double noiseDist) : Sensor(noiseMean, noiseDist)
{
    type = SensorType::LIDARSENSOR;
    int index = 0;
    for(int i = 1; i < 180; i += 2)
    {
        slopes[index] = 1/(tan(i * (M_PI/180)));
        ++index;
    }
    wallsInWorld[0] = std::make_shared<Model::Wall>(Point(0,0), Point(0,1024));
    wallsInWorld[1] = std::make_shared<Model::Wall>(Point(0,0), Point(1024,0));
    wallsInWorld[2] = std::make_shared<Model::Wall>(Point(1024,0), Point(1024,1024));
    wallsInWorld[3] = std::make_shared<Model::Wall>(Point(0,1024), Point(1024,1024));
}


std::shared_ptr<Measurement> LidarSensor::measure(const World& world)
{
    std::shared_ptr<LidarMeasurement> measurement = std::make_shared<LidarMeasurement>();
    Point robotCenter = world.getPosRobot();
    measurement->distances = wallDistance(robotCenter, world.getWalls());
    return measurement;
}

void LidarSensor::init()
{

}

LidarSensor::~LidarSensor()
{
}

std::array<double, 180> LidarSensor::wallDistance(const Point& from, std::vector<Model::WallPtr> walls)
{
    std::array<double, 180> distances;
    for(int i = 0; i < 90; ++i)
    {
        double b = slopes[i]*from.x-from.y;
        Point center(15000 , static_cast<int>(round(slopes[i]*15000+b)));
        for(auto& wall: wallsInWorld)
        {
            Point detectWall = Utils::Shape2DUtils::getIntersection(wall->getPoint1(), wall->getPoint2(), from, center);
            if(detectWall.x != -1 && detectWall.y != -1 )
            {
                if(detectWall.x >= std::min(wall->getPoint1().x, wall->getPoint2().x) && detectWall.x <= std::max(wall->getPoint1().x, wall->getPoint2().x) &&
                   detectWall.y >= std::min(wall->getPoint1().y, wall->getPoint2().y) &&  detectWall.y <= std::max(wall->getPoint1().y, wall->getPoint2().y))
                {
                    double distanceX = sqrt(pow(abs(detectWall.x - from.x), 2));
                    double distanceY = sqrt(pow(abs(detectWall.y - from.y), 2));
					double distance = distanceX + distanceY;

                    int index = 0;
                    if((from.y - detectWall.y > 0 && slopes[i] > 0) || (from.y - detectWall.y < 0 && slopes[i] < 0) || (from.y - detectWall.y == 0 && from.x - detectWall.x > 0))
                    {
                        index = i;
                    } else {
                        index = 90+i;
                    }
                    distances[index] = distance+measurementNoise();
                }
            }
        }
        bool wallDetected = false;
        for(auto& wall: walls)
        {
            Point detectWall = Utils::Shape2DUtils::getIntersection(wall->getPoint1(), wall->getPoint2(), from, center);
            if(detectWall.x != -1 && detectWall.y != -1)
            {
                if(detectWall.x >= std::min(wall->getPoint1().x, wall->getPoint2().x) && detectWall.x <= std::max(wall->getPoint1().x, wall->getPoint2().x) &&
                   detectWall.y >= std::min(wall->getPoint1().y, wall->getPoint2().y) &&  detectWall.y <= std::max(wall->getPoint1().y, wall->getPoint2().y))
                {
                    double distanceX = sqrt(pow(abs(detectWall.x - from.x), 2));
                    double distanceY = sqrt(pow(abs(detectWall.y - from.y), 2));
					double distance = distanceX + distanceY;
                    int index = 0;
                    if((from.y - detectWall.y > 0 && slopes[i] > 0) || (from.y - detectWall.y < 0 && slopes[i] < 0) || (from.y - detectWall.y == 0 && from.x - detectWall.x > 0))
                    {
                        index = i;
                    } else {
                        index = 90+i;
                    }
                    if(wallDetected && distance < distances[index])
                    {
                        distances[index] = distance+measurementNoise();
                    } else {
                        distances[index] = distance+measurementNoise();
                        wallDetected = true;
                    }
                }
            }
        }
    }
    return distances;
}

CompasMeasurement::CompasMeasurement()
{
}

void CompasSensor::init()
{

}

CompasMeasurement::~CompasMeasurement()
{
}

double CompasMeasurement::ParticleWeight(const Particle& particle) const
{
    return 0;
}

CompasSensor::CompasSensor(double noiseMean, double noiseDist) : Sensor(noiseMean, noiseDist)
{
    type = SensorType::COMPASSENSOR;
}

std::shared_ptr<Measurement> CompasSensor::measure(const World& world)
{
    double orientation = world.getOrientation();
    std::shared_ptr<CompasMeasurement> measurement = std::make_shared<CompasMeasurement>();
    measurement->orientation = (orientation * 180/M_PI) + measurementNoise();
    return measurement;
}


CompasSensor::~CompasSensor()
{
}

OdometerMeasurement::OdometerMeasurement()
{
}

OdometerMeasurement::~OdometerMeasurement()
{
}

double OdometerMeasurement::ParticleWeight(const Particle& particle) const
{
    return 0;
}

OdometerSensor::OdometerSensor(double noiseMean, double noiseDist) : Sensor(noiseMean, noiseDist)
{
    type = SensorType::ODOMETERSENSOR;
}

std::shared_ptr<Measurement> OdometerSensor::measure(const World& world)
{
    Point newPos = world.getPosRobot();
    std::shared_ptr<OdometerMeasurement> measurement = std::make_shared<OdometerMeasurement>();
    measurement->distance = sqrt(pow(newPos.x - prevPos.x, 2) + pow(newPos.y - prevPos.y, 2)) + measurementNoise();
    prevPos = newPos;
    return measurement;
}

void OdometerSensor::init()
{
    prevPos = World::getWorld().getPosRobot();
}

OdometerSensor::~OdometerSensor()
{
}
