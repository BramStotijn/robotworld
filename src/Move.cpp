#include "Move.hpp"
#include "World.hpp"
#include <cmath>

Move::Move() :
        noise(0, 1)
{
    type = ActuatorType::DRIVE;
}

Move::~Move()
{
}

Point Move::moveToPos(const Point& originalPos, double orientation, double distance)
{
    Point newPos = originalPos;
    newPos.x += static_cast<int>(round(distance * cos(orientation)));
    newPos.y += static_cast<int>(round(distance * sin(orientation)));
    xNoise = noise();
    yNoise = noise();

    if(xNoise < 0)
    {
    	newPos.x += std::max(static_cast<int>(xNoise), -2);
    }
    else
    {
    	newPos.x += std::min(static_cast<int>(xNoise), 2);
    }

    if(yNoise < 0)
    {
    	newPos.y += std::max(static_cast<int>(yNoise), -2);
    }
    else
    {
    	newPos.y += std::min(static_cast<int>(yNoise), 2);
    }

    return newPos;
}

const RandomDistribution<std::normal_distribution<double>, double>& Move::getNoise() const
{
    return noise;
}

void Move::handleCommand(std::shared_ptr<BaseCommand> anCommand)
{
    std::shared_ptr<driveCommands> command = std::dynamic_pointer_cast<driveCommands>(anCommand);

    Point newPos = World::getWorld().getPosRobot();

    newPos = moveToPos(newPos, command->orientation, command->distance);

    World::getWorld().setOrientation(command->orientation);

    World::getWorld().setPosRobot(newPos);

}

driveCommands::driveCommands( double anOrientation, double aDistance) :
        orientation(anOrientation),
        distance(aDistance)
{
}



driveCommands::~driveCommands()
{
}
