#ifndef ROBOTWORLDWOR_PARTICLEFILTER_HPP
#define ROBOTWORLDWOR_PARTICLEFILTER_HPP

#include <array>
#include <map>
#include <memory>
#include <random>
#include <algorithm>
#include <type_traits>
#include <functional>
#include "RandomDistrib.hpp"
#include "Particle.hpp"
#include "Sensor.hpp"

template<std::size_t n_particles, std::size_t n_measurements>
class ParticleFilter
{
    static_assert(n_particles > 0, "n_particles must be greater than 0");
    static_assert(n_measurements > 0, "n_measurements must be greater than 0");
public:
    typedef std::array<Particle, n_particles> particleList;
    typedef std::function<void(Particle&)> particleFunction;
    typedef std::array<std::shared_ptr<Measurement>, n_measurements> measurementsPtrList;
    ParticleFilter();
    ~ParticleFilter();
    void filter(particleList& particle_dist, particleFunction actionDistribFunc, measurementsPtrList& measurements);
    particleList initializeBelief() const;
    Point posiblePos(const particleList& particle_dist) const;
};

template<std::size_t n_particles, std::size_t n_measurements>
ParticleFilter<n_particles, n_measurements>::ParticleFilter(/* args */)
{
}

template<std::size_t n_particles, std::size_t n_measurements>
ParticleFilter<n_particles, n_measurements>::~ParticleFilter()
{
}

struct particleWeight
{
    double targetWeight;
	double startWeight;
    particleWeight(double weight) : targetWeight(weight), startWeight(0){}
    bool operator()(const Particle& part)
    {
        bool found = (targetWeight <= part.weight && targetWeight > startWeight);
        startWeight = part.weight;
        return found;
    }
};


template<std::size_t n_particles, std::size_t n_measurements>
void ParticleFilter<n_particles, n_measurements>::filter(particleList& particle_dist, particleFunction actionDistribFunc, measurementsPtrList& measurements)
{
    double weightTotal = 0;
    std::array<Particle, n_particles> _X = particle_dist;
    for(auto& particle : _X)
    {
        actionDistribFunc(particle);

        for(auto& measurement : measurements)
        {
            particle.weight += measurement->ParticleWeight(particle);
        }
        weightTotal += particle.weight;
        particle.weight = weightTotal;
    }
    RandomDistribution<std::uniform_real_distribution<double>, double> dist(0, weightTotal);
    for(auto& particle : particle_dist)
    {
        double randomWeight = dist();
        particle = (*std::find_if(_X.begin(), _X.end(), particleWeight(randomWeight)));
        particle.weight = 0.1;
    }
}
template<std::size_t n_particles, std::size_t n_measurements>
std::array<Particle, n_particles> ParticleFilter<n_particles, n_measurements>::initializeBelief() const
{
    RandomDistribution<std::uniform_int_distribution<int>, int> distancePos(0, 1024);
    RandomDistribution<std::uniform_real_distribution<double>, double> distanceOrientation(-M_PI, M_PI);
    particleList returnList;
    for(Particle& particle : returnList)
    {
        particle.position.x = distancePos();
        particle.position.y = distancePos();
        particle.orientation = distanceOrientation();
        particle.weight = 0.1;
    }
    return returnList;
}

struct pointLTCompare
{
    bool operator()(const Point& pt1, const Point& pt2) const
    {
        return(pt1.x < pt2.x && pt1.y < pt2.y);
    }
};

template<std::size_t n_particles, std::size_t n_measurements>
Point ParticleFilter<n_particles, n_measurements>::posiblePos(const particleList& particle_dist) const
{
    std::map<Point, unsigned long long, pointLTCompare> particlePosCount;
    for(auto& particle : particle_dist)
    {
        particlePosCount[particle.position]++;
    }
    std::pair<Point, unsigned long long> bestPoint = std::make_pair(Point(0,0), 0);
    for(auto& numberPointPair : particlePosCount)
    {
        if(bestPoint.second < numberPointPair.second)
        {
            bestPoint = numberPointPair;
        }
    }
    return bestPoint.first;
}


#endif //ROBOTWORLDWOR_PARTICLEFILTER_HPP
