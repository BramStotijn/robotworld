#ifndef ROBOTWORLDWOR_MOVE_HPP
#define ROBOTWORLDWOR_MOVE_HPP

#include "Point.hpp"
#include "RandomDistrib.hpp"
#include <memory>
#include "Actuator.hpp"

#define STEP_SIZE 5

enum class ActuatorType : unsigned int
{
    DRIVE
};

struct driveCommands : public BaseCommand
{
    driveCommands(
            double anOrientation,
            double aDistance
    );

    virtual ~driveCommands();
    double orientation;
    double distance;
};

class Move : public Actuator
{
private:
    RandomDistribution<std::normal_distribution<double>, double> noise;
    double xNoise;
    double yNoise;
public:
    Move();
    virtual ~Move();
    Point moveToPos(const Point& originalPos, double orientation, double distance);
    const RandomDistribution<std::normal_distribution<double>, double>& getNoise() const;
    void handleCommand(std::shared_ptr<BaseCommand> anCommand);
};

#endif //ROBOTWORLDWOR_MOVE_HPP
