#include <sstream>
#include "Logger.hpp"
#include "Shape2DUtils.hpp"
#include "NormalLine.hpp"

namespace Model
{

    NormalLine::NormalLine(  const std::string& colour) : connect(true), colour(colour)
    {
    }

    NormalLine::NormalLine( const Point& aPoint1,
                const Point& aPoint2,
                const std::string& colour) : connect(true), colour(colour),  points({aPoint1, aPoint2})
    {
    }

    NormalLine::~NormalLine()
    {
    }

    void NormalLine::setConnected(bool connected)
    {
        connect = connected;
    }

    bool NormalLine::isConnected()
    {
        return connect;
    }

    void NormalLine::clear()
    {
        std::size_t pointsSize = points.size();
        points.clear();
        points.reserve(pointsSize);
    }

    const std::vector<Point>& NormalLine::getPoints()
    {
        return points;
    }

    const std::string& NormalLine::getColour()
    {
        return colour;
    }

    Point NormalLine::getPointAt(std::size_t index) const
    {
        if(index < points.size()-1)
        {
            return points[index];
        }
        return wxDefaultPosition;
    }

    void NormalLine::setPointAt( const Point& aPoint, std::size_t index,
                           bool aNotifyObservers)
    {
        if(index < points.size()-1)
        {
            points[index] = aPoint;
        } else {
            points.push_back(aPoint);
        }
    }

    void NormalLine::addPoint(const Point& aPoint, bool aNotifyObservers)
    {
        points.push_back(aPoint);
    }
} // namespace Model
