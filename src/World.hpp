#ifndef ROBOTWORLDWOR_WORLD_HPP
#define ROBOTWORLDWOR_WORLD_HPP

#include "AbstractAgent.hpp"
#include "Sensors.hpp"
#include "Wall.hpp"
#include <vector>
#include <thread>
#include "Move.hpp"
#include "NormalLine.hpp"

class World : public Base::Notifier
{
private:
    std::vector<std::shared_ptr<Sensor>> sensors;
    std::vector<std::shared_ptr<Actuator>> actuators;
    Model::LinePtr driven;
    Point robotPos;
    double robotOrientation;
    std::mutex getLinesLock;
    World();
public:
    World(const World&);
    virtual ~World();
    static World& getWorld();
    void setPosRobot(const Point& pos);
    void setOrientation(double orientation);
    const Point& getPosRobot() const;
    double getOrientation() const;
    void clearLines();
    const std::vector<Model::WallPtr>& getWalls() const;
    std::vector<Model::LinePtr> getLines();
    void attachActuator(std::shared_ptr<Actuator> actuator);
    void attachSensor(std::shared_ptr<Sensor> sensor);
    std::shared_ptr<Sensor> getSensor(SensorType sensor) const;
    std::shared_ptr<Measurement> measureSensor(SensorType sensor);
    void actuate(ActuatorType actuator, std::shared_ptr<BaseCommand> command);
    void initSensors();
    std::shared_ptr<Actuator> getActuator(ActuatorType actuator);
};

#endif //ROBOTWORLDWOR_WORLD_HPP
