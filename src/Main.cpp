#include "Config.hpp"

#include "MainApplication.hpp"

#include "Logger.hpp"
#include "Trace.hpp"
#include "FileTraceFunction.hpp"

#include <iostream>
#include <string>
#include <stdexcept>

#include "MainApplication.hpp"
#include "Move.hpp"
#include "World.hpp"
#include "Widgets.hpp"

int main( 	int argc,
			char* argv[])
{
	try
	{
		std::shared_ptr<LidarSensor> lidar = std::make_shared<LidarSensor>(0, 10);
		std::shared_ptr<OdometerSensor> odometer = std::make_shared<OdometerSensor>(0, 1);
		std::shared_ptr<CompasSensor> compas = std::make_shared<CompasSensor>(0, 2);
		std::shared_ptr<Move> driver = std::make_shared<Move>();
		World::getWorld().attachSensor(lidar);
		World::getWorld().attachSensor(odometer);
		World::getWorld().attachSensor(compas);
		World::getWorld().attachActuator(driver);
		// Call the wxWidgets main variant
		// This will actually call Application
		int result = runGUI( argc, argv);
		return result;
	}
	catch (std::exception& e)
	{
		Application::Logger::log( __PRETTY_FUNCTION__ + std::string(": ") + e.what());
		std::cerr << __PRETTY_FUNCTION__ << ": " << e.what() << std::endl;
	}
	catch (...)
	{
		Application::Logger::log( __PRETTY_FUNCTION__ + std::string(": unknown exception"));
		std::cerr << __PRETTY_FUNCTION__ << ": unknown exception" << std::endl;
	}
	return 0;
}
