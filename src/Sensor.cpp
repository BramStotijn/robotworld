#include "Sensor.hpp"

Measurement::Measurement()
{
}

Measurement::~Measurement()
{
}

Sensor::Sensor(double noiseMean, double noiseDist):
        measurementNoise(RandomDistribution<std::normal_distribution<double>, double>(noiseMean,noiseDist))
{
}

SensorType Sensor::getType() const
{
    return type;
}

const NoiseNormalDoubleDist& Sensor::getNoise() const
{
    return measurementNoise;
}

Sensor::~Sensor()
{
}
