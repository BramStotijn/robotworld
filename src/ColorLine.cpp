#include <algorithm>
#include <sstream>

#include "Logger.hpp"
#include "Shape2DUtils.hpp"
#include "Size.hpp"
#include "ColorLine.hpp"

namespace View
{

    ColorLine::ColorLine(Model::LinePtr model, const std::vector<Point>& linePoints, const std::string& color,
                                         int lineThiccness, const std::string& aTitle) :
            		Shape(model), linePoints(linePoints), color(color), title( aTitle), lineThiccness( lineThiccness)
    {
    }

    ColorLine::~ColorLine()
    {
    }

    void ColorLine::draw( wxDC& dc)
    {
        dc.SetPen( wxPen( WXSTRING( color), lineThiccness, wxPENSTYLE_SOLID));
        Point prevPoint = linePoints[0];
        for(const Point& point : linePoints)
        {
            if(connect)
            {
                dc.DrawLine(prevPoint, point);
                prevPoint = point;
            } else {
                dc.DrawPoint(point);
            }

        }
    }

    bool ColorLine::occupies( const Point& aPoint) const
    {

        Point prevPoint = linePoints[0];
        for(const Point& point : linePoints)
        {
            if(Utils::Shape2DUtils::isOnLine(prevPoint, point, aPoint, 1))
            {
                return true;
            }
            prevPoint = point;
        }
        return false;
    }

    Point ColorLine::getCentre() const
    {
        return linePoints[linePoints.size()/2];
    }

    void ColorLine::setCentre( const Point& aPoint)
    {
    }

    void ColorLine::setTitle( const std::string& aTitle)
    {
        title = aTitle;
    }

    std::string ColorLine::getTitle() const
    {
        return title;
    }

	void ColorLine::handleActivated()
	{
	}

	void ColorLine::handleSelection()
	{
	}

} // namespace View
