#ifndef ROBOTWORLDWOR_COLORLINE_HPP
#define ROBOTWORLDWOR_COLORLINE_HPP


#include "Config.hpp"
#include "Shape.hpp"
#include "RectangleShape.hpp"

#include <memory>
#include <string>
#include <vector>
#include "NormalLine.hpp"


namespace View
{
    /**
     *
     */
    class ColorLine : public Shape
    {
    public:
        /**
         *
         */
        ColorLine( Model::LinePtr model,
                           const std::vector<Point>& linePoints = {Point(0,0)},
                           const std::string& color = "BLACK",
                           int lineThiccness = 1,
                           const std::string& aTitle = "");
        /**
         *
         */
        virtual ~ColorLine();
        /**
         * Notifier for observer
         */
        virtual void handleNotification(){}
        /**
         *
         */
        virtual void draw( wxDC& dc);
        /**
         *
         * @param aPoint
         * @return Check if point is in wall
         */
        virtual bool occupies( const Point& aPoint) const;

        void setLine(const std::vector<Point>& points)
        {
            linePoints = points;
        }

        void setColor(const std::string& aColor)
        {
            color = aColor;
        }

        void setConnected(bool connected)
        {
            connect = connected;
        }

        virtual void setCentre( const Point& aPoint);

        void setTitle( const std::string& aTitle);


        virtual Point getCentre() const;

        std::string getTitle() const;

        virtual void handleActivated();
		
        virtual void handleSelection();

        Model::LinePtr getLine() const
        {
            return std::dynamic_pointer_cast<Model::NormalLine>(getModelObject());
        }

    private:
        bool connect;
        int lineThiccness;
        std::string title;
        std::string color;
        std::vector<Point> linePoints;
    };//	class ColorLine
} // namespace View


#endif //ROBOTWORLDWOR_COLORLINE_HPP
