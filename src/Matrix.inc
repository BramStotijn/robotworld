#include <cassert>
#include <stdexcept>
#include <numeric>
#include <cmath>
#include <utility>
#include <iomanip>

/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N >::Matrix( T value)
{
	for (std::size_t row = 0; row < M; ++row)
	{
		for (std::size_t column = 0; column < N; ++column)
		{
			matrix.at( row).at( column) = value;
		}
	}
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N >::Matrix( const std::initializer_list< T >& aList)
{
	// Check the arguments
	assert( aList.size() == M * N);

	auto row_iter = aList.begin();
	for (std::size_t row = 0; row < M; ++row)
	{
		for (std::size_t column = 0; column < N; ++column, ++row_iter)
		{
			matrix.at( row).at( column) = *row_iter;
		}
	}
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N >::Matrix( const std::initializer_list< std::initializer_list< T > >& aList)
{
	// Check the arguments, the static assert assures that there is at least 1 M and 1 N!
	assert( aList.size() == M && (*aList.begin()).size() == N);

	auto row_iter = aList.begin();
	for (std::size_t row = 0; row < aList.size(); ++row, ++row_iter)
	{
		auto column_iter = (*row_iter).begin();
		for (std::size_t column = 0; column < (*row_iter).size(); ++column, ++column_iter)
		{
			matrix.at( row).at( column) = *column_iter;
		}
	}
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N >::Matrix( const Matrix< T, M, N >& aMatrix) :
				matrix( aMatrix.matrix)
{
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
std::array< T, N >& Matrix< T, M, N >::at( std::size_t aRowIndex)
{
	return matrix.at( aRowIndex);
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
const std::array< T, N >& Matrix< T, M, N >::at( std::size_t aRowIndex) const
{
	return matrix.at( aRowIndex);
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
T& Matrix< T, M, N >::at( 	std::size_t aRowIndex,
							std::size_t aColumnIndex)
{
	return matrix.at( aRowIndex).at( aColumnIndex);
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
const T& Matrix< T, M, N >::at( std::size_t aRowIndex,
								std::size_t aColumnIndex) const
{
	return matrix.at( aRowIndex).at( aColumnIndex);
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
std::array< T, N >& Matrix< T, M, N >::operator[]( std::size_t aRowIndex)
{
	return matrix[aRowIndex];
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
const std::array< T, N >& Matrix< T, M, N >::operator[]( std::size_t aRowIndex) const
{
	return matrix[aRowIndex];
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N >& Matrix< T, M, N >::operator=( const Matrix< T, M, N >& rhs)
{
	if (this != &rhs)
	{
		matrix = rhs.matrix;
	}
	return *this;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
bool Matrix< T, M, N >::operator==( const Matrix< T, M, N >& rhs) const
{
	return matrix == rhs.matrix;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
template< class T2 >
Matrix< T, M, N >& Matrix< T, M, N >::operator*=( const T2& scalar)
{
	static_assert( std::is_arithmetic<T2>::value, "Value T2 must be arithmetic, see http://en.cppreference.com/w/cpp/types/is_arithmetic");

	for (std::size_t row = 0; row < M; ++row)
	{
		for (std::size_t column = 0; column < N; ++column)
		{
			matrix.at( row).at( column) *= scalar;
		}
	}
	return *this;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
template< class T2 >
Matrix< T, M, N > Matrix< T, M, N >::operator*( const T2& scalar) const
{
	static_assert( std::is_arithmetic<T2>::value, "Value T2 must be arithmetic, see http://en.cppreference.com/w/cpp/types/is_arithmetic");

	Matrix< T, M, N > result( *this);
	return result *= scalar;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
template< class T2 >
Matrix< T, M, N >& Matrix< T, M, N >::operator/=( const T2& aScalar)
{
	static_assert( std::is_arithmetic<T2>::value, "Value T2 must be arithmetic, see http://en.cppreference.com/w/cpp/types/is_arithmetic");

	for (std::size_t row = 0; row < M; ++row)
	{
		for (std::size_t column = 0; column < N; ++column)
		{
			matrix.at( row).at( column) /= aScalar;
		}
	}
	return *this;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
template< class T2 >
Matrix< T, M, N > Matrix< T, M, N >::operator/( const T2& aScalar) const
{
	static_assert( std::is_arithmetic<T2>::value, "Value T2 must be arithmetic, see http://en.cppreference.com/w/cpp/types/is_arithmetic");

	Matrix< T, M, N > result( *this);
	return result /= aScalar;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N >& Matrix< T, M, N >::operator+=( const Matrix< T, M, N >& rhs)
{
	for (std::size_t row = 0; row < M; ++row)
	{
		for (std::size_t column = 0; column < N; ++column)
		{
			matrix[row][column] += rhs.at( row, column);
		}
	}
	return *this;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N > Matrix< T, M, N >::operator+( const Matrix< T, M, N >& rhs) const
{
	Matrix< T, M, N > result( *this);
	return result += rhs;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N >& Matrix< T, M, N >::operator-=( const Matrix< T, M, N >& rhs)
{
	for (std::size_t row = 0; row < M; ++row)
	{
		for (std::size_t column = 0; column < N; ++column)
		{
			matrix[row][column] -= rhs.at( row, column);
		}
	}
	return *this;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N > Matrix< T, M, N >::operator-( const Matrix< T, M, N >& rhs) const
{
	Matrix< T, M, N > result( *this);
	return result -= rhs;
}
/**
 * (M, N) * (N, P) -> (M, P)
 */
template< typename T, std::size_t M, std::size_t N >
template< std::size_t columns >
Matrix< T, M, columns > Matrix< T, M, N >::operator*( const Matrix< T, N, columns >& rhs) const
{

	if(this->getColumns() != rhs.getRows())
	{
		throw std::logic_error("columns of left matrix are not equal to rows of right matrix");
	}
	Matrix< T, M, columns > result;
	for(std::size_t lhsRow = 0; lhsRow < this->getRows(); ++lhsRow)
	{
		for(std::size_t rhsCol = 0; rhsCol < rhs.getColumns(); ++rhsCol)
		{
			for(std::size_t rhsRow = 0; rhsRow < rhs.getRows(); ++rhsRow)
			{
				result[lhsRow][rhsCol] += (matrix[lhsRow][rhsRow] * rhs[rhsRow][rhsCol]);
			}
		}
	}

	return result;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, N, M > Matrix< T, M, N >::transpose() const
{
	Matrix< T, N, M > result;
	for(std::size_t row = 0; row < M; ++row)
	{
		for(std::size_t col = 0; col < N; ++col)
		{
			result[col][row] = matrix[row][col];
		}
	}
	return result;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N > Matrix< T, M, N >::identity() const
{
	if(M != N)
	{
		throw std::logic_error("cannot get identity matrix of non block matrix");
	}
	Matrix< T, N, M > result;
	for(std::size_t row = 0; row < M; ++row)
	{
		result[row][row] = 1;
	}
	return result;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N > Matrix< T, M, N >::gauss() const
{
	Matrix< T, M, N > result;
	result = *this;
	for(std::size_t row = 0; row < M; ++row)
	{
		T divider = result[row][row];
		if(divider == 0)
		{
			std::size_t swap_row = 0;
			T abs_max = 0;
			for(std::size_t srow = row; srow < M; ++srow)
			{
				if(abs_max < abs(result[srow][srow]))
				{
					abs_max = abs(result[srow][srow]);
					swap_row = srow;
				}
			}
			std::swap(result[row], result[swap_row]);
			divider = result[row][row];
		}
		for(std::size_t column = 0; column < N; ++column)
		{
			result[row][column] /= divider;
		}
		for(std::size_t new_row = 0; new_row < M; ++new_row)
		{
			if(new_row > row)
			{
				T multiplication_val = result[new_row][row]/result[row][row];
				for(std::size_t column = 0; column < N; ++column)
				{
					result[new_row][column] -= (result[row][column]*multiplication_val);
				}
			}
		}
	}
	return result;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N > Matrix< T, M, N >::gaussJordan() const
{
	Matrix< T, M, N > result;
	result = *this;
	for(std::size_t row = 0; row < M; ++row)
	{
		T divider = result[row][row];
		if(divider == 0)
		{
			std::size_t swap_row = 0;
			T abs_max = 0;
			for(std::size_t srow = row; srow < M; ++srow)
			{
				if(abs_max < fabs(result[srow][srow]))
				{
					abs_max = fabs(result[srow][srow]);
					swap_row = srow;
				}
			}
			std::swap(result[row], result[swap_row]);
			divider = result[row][row];
		}
		for(std::size_t column = 0; column < N; ++column)
		{
			result[row][column] /= divider;
		}
		for(std::size_t new_row = 0; new_row < M; ++new_row)
		{
			if(new_row != row)
			{
				T multiplication_val = result[new_row][row]/result[row][row];
				for(std::size_t column = 0; column < N; ++column)
				{
					result[new_row][column] -= (result[row][column]*multiplication_val);
				}
			}
		}
	}
	return result;
}
/**
 *

 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, 1 > Matrix< T, M, N >::solve() const
{
	// TODO Implement this function
	throw(std::logic_error("not implemented"));
	Matrix < T, M, 1 > result;
	return result;
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
Matrix< T, M, N > Matrix< T, M, N >::inverse() const
{
	Matrix<T, M, N*2> dual_matrix;
	Matrix<T, M, N> identity = this->identity();
	for (std::size_t row = 0; row < M; ++row)
	{
		for (std::size_t column = 0; column < N; ++column)
		{
			dual_matrix[row][column] = (*this)[row][column];
			dual_matrix[row][column+N] = identity[row][column];
		}
	}
	dual_matrix = dual_matrix.gaussJordan();
	Matrix<T, M, N> result;
	for (std::size_t row = 0; row < M; ++row)
	{
		for (std::size_t column = 0; column < N; ++column)
		{
			result[row][column] = dual_matrix[row][column+N];
		}
	}
	//if(result * (*this) == this->identity()){
		return result;
	/*}
	return Matrix<T, M, N>(0);*/
}
/**
 *
 */
template< class T, std::size_t M, std::size_t N >
std::string Matrix< T, M, N >::to_string() const
{
	std::string result = "Matrix<" + std::to_string( N) + "," + std::to_string( M) + ">\n{\n";
	for (std::size_t i = 0; i < M; ++i)
	{
		for (std::size_t j = 0; j < N; ++j)
		{
			result += std::to_string( matrix[i][j]) + ",";
		}
		result += "\n";
	}
	result += "}";
	return result;
}
/**
 *
 */
template< typename T, const std::size_t N >
bool equals(	const Matrix< T, 1, N >& lhs,
				const Matrix< T, 1, N >& rhs,
				const T aPrecision /*= std::numeric_limits<T>::epsilon()*/,
				const unsigned long aFactor /*= 1*/)
{
	for(std::size_t col = 0; col < N; ++col)
	{
		if( lhs[0][col] > (rhs[0][col] + aPrecision*aFactor) ||
			lhs[0][col] < (rhs[0][col] - aPrecision*aFactor))
		{
			return false;
		}
	}
	return true;
}
/**
 *
 */
template< typename T, const std::size_t M >
bool equals(	const Matrix< T, M, 1 >& lhs,
				const Matrix< T, M, 1 >& rhs,
				const T aPrecision /*= std::numeric_limits<T>::epsilon()*/,
				const unsigned long aFactor /*= 1*/)
{
	for(std::size_t row = 0; row < M; ++row)
	{
		if( lhs[row][0] > (rhs[row][0] + aPrecision*aFactor) ||
			lhs[row][0] < (rhs[row][0] - aPrecision*aFactor))
		{
			return false;
		}
	}
	return true;
}
/**
 *
 */
template< typename T, const std::size_t M, const std::size_t N >
bool equals(	const Matrix< T, M, N >& lhs,
				const Matrix< T, M, N >& rhs,
				const T aPrecision /*= std::numeric_limits<T>::epsilon()*/,
				const unsigned long aFactor /*= 1*/)
{
	for(std::size_t row = 0; row < M; ++row)
	{
		for(std::size_t col = 0; col < N; ++col)
		{
			if( lhs[row][col] > (rhs[row][col] + aPrecision*aFactor) ||
				lhs[row][col] < (rhs[row][col] - aPrecision*aFactor))
			{
				std::cout.precision(20);
				return false;
			}
		}

	}
	return true;
}
