#ifndef ROBOTWORLDWOR_SENSOR_HPP
#define ROBOTWORLDWOR_SENSOR_HPP

#include "RandomDistrib.hpp"
#include <memory>
#include "Particle.hpp"

class World;

enum class SensorType : unsigned int;

typedef RandomDistribution<std::normal_distribution<double>, double> NoiseNormalDoubleDist;

struct Measurement
{
    Measurement();
    virtual ~Measurement();
    virtual double ParticleWeight(const Particle& particle) const = 0;
};

class Sensor
{
public:
    Sensor(double noiseMean, double noiseDist);
    virtual ~Sensor();
    virtual std::shared_ptr<Measurement> measure(const World& world) = 0;
    virtual void init() = 0;
    SensorType getType() const;
    const NoiseNormalDoubleDist& getNoise() const;
protected:
    SensorType type;
    NoiseNormalDoubleDist measurementNoise;
};



#endif //ROBOTWORLDWOR_SENSOR_HPP
