#include "Robot.hpp"

#include "Client.hpp"
#include "CommunicationService.hpp"
#include "Goal.hpp"
#include "LaserDistanceSensor.hpp"
#include "Logger.hpp"
#include "MainApplication.hpp"
#include "MathUtils.hpp"
#include "Message.hpp"
#include "MessageTypes.hpp"
#include "RobotWorld.hpp"
#include "Server.hpp"
#include "Shape2DUtils.hpp"
#include "Wall.hpp"
#include "WayPoint.hpp"

#include <chrono>
#include <ctime>
#include <sstream>
#include <thread>
#include <iostream>

#include "World.hpp"
#include "Sensors.hpp"

namespace Model
{
	/**
	 *
	 */
	Robot::Robot() : name(""),
					 size(DefaultSize),
					 position(DefaultPosition),
					 front(0, 0),
					 speed(0.0),
					 acting(false),
					 driving(false),
					 communicating(false)
	{
		std::shared_ptr<AbstractSensor> laserSensor(new LaserDistanceSensor(this));
		attachSensor(laserSensor);
		World::getWorld().setPosRobot(position);
	}
	/**
	 *
	 */
	Robot::Robot(const std::string &aName) : name(aName),
											 size(DefaultSize),
											 position(DefaultPosition),
											 front(0, 0),
											 speed(0.0),
											 acting(false),
											 driving(false),
											 communicating(false)
	{
		std::shared_ptr<AbstractSensor> laserSensor(new LaserDistanceSensor(this));
		attachSensor(laserSensor);
		World::getWorld().setPosRobot(position);
	}
	/**
	 *
	 */
	Robot::Robot(const std::string &aName,
				 const Point &aPosition) : name(aName),
										   size(DefaultSize),
										   position(aPosition),
										   front(0, 0),
										   speed(0.0),
										   acting(false),
										   driving(false),
										   communicating(false)
	{
		std::shared_ptr<AbstractSensor> laserSensor(new LaserDistanceSensor(this));
		attachSensor(laserSensor);
		World::getWorld().setPosRobot(aPosition);
	}
	/**
	 *
	 */
	Robot::~Robot()
	{
		if (driving)
		{
			stopDriving();
		}
		if (acting)
		{
			stopActing();
		}
		if (communicating)
		{
			stopCommunicating();
		}
	}
	/**
	 *
	 */
	void Robot::setName(const std::string &aName,
						bool aNotifyObservers /*= true*/)
	{
		name = aName;
		if (aNotifyObservers == true)
		{
			notifyObservers();
		}
	}
	/**
	 *
	 */
	Size Robot::getSize() const
	{
		return size;
	}
	/**
	 *
	 */
	void Robot::setSize(const Size &aSize,
						bool aNotifyObservers /*= true*/)
	{
		size = aSize;
		if (aNotifyObservers == true)
		{
			notifyObservers();
		}
	}
	/**
	 *
	 */
	void Robot::setPosition(const Point &aPosition,
							bool aNotifyObservers /*= true*/)
	{
		position = aPosition;
		if (aNotifyObservers == true)
		{
			notifyObservers();
		}
	}
	/**
	 *
	 */
	BoundedVector Robot::getFront() const
	{
		return front;
	}
	/**
	 *
	 */
	void Robot::setFront(const BoundedVector &aVector,
						 bool aNotifyObservers /*= true*/)
	{
		front = aVector;
		if (aNotifyObservers == true)
		{
			notifyObservers();
		}
	}
	/**
	 *
	 */
	float Robot::getSpeed() const
	{
		return speed;
	}
	/**
	 *
	 */
	void Robot::setSpeed(float aNewSpeed,
						 bool aNotifyObservers /*= true*/)
	{
		speed = aNewSpeed;
		if (aNotifyObservers == true)
		{
			notifyObservers();
		}
	}
	/**
	 *
	 */
	void Robot::startActing()
	{
		acting = true;
		std::thread newRobotThread([this]
								   { startDriving(); });
		robotThread.swap(newRobotThread);
	}
	/**
	 *
	 */
	void Robot::stopActing()
	{
		acting = false;
		driving = false;
		robotThread.join();
	}
	/**
	 *
	 */
	void Robot::startDriving()
	{
		driving = true;

		drive();
	}
	/**
	 *
	 */
	void Robot::stopDriving()
	{
		driving = false;
	}
	/**
	 *
	 */
	void Robot::startCommunicating()
	{
		if (!communicating)
		{
			communicating = true;

			std::string localPort = "12345";
			if (Application::MainApplication::isArgGiven("-local_port"))
			{
				localPort = Application::MainApplication::getArg("-local_port").value;
			}

			if (Messaging::CommunicationService::getCommunicationService().isStopped())
			{
				TRACE_DEVELOP("Restarting the Communication service");
				Messaging::CommunicationService::getCommunicationService().restart();
			}

			server = std::make_shared<Messaging::Server>(static_cast<unsigned short>(std::stoi(localPort)),
														 toPtr<Robot>());
			Messaging::CommunicationService::getCommunicationService().registerServer(server);
		}
	}
	/**
	 *
	 */
	void Robot::stopCommunicating()
	{
		if (communicating)
		{
			communicating = false;

			std::string localPort = "12345";
			if (Application::MainApplication::isArgGiven("-local_port"))
			{
				localPort = Application::MainApplication::getArg("-local_port").value;
			}

			Messaging::Client c1ient("localhost",
									 static_cast<unsigned short>(std::stoi(localPort)),
									 toPtr<Robot>());
			Messaging::Message message(Messaging::StopCommunicatingRequest, "stop");
			c1ient.dispatchMessage(message);
		}
	}
	/**
	 *
	 */
	Region Robot::getRegion() const
	{
		Point translatedPoints[] = {getFrontRight(), getFrontLeft(), getBackLeft(), getBackRight()};
		return Region(4, translatedPoints);
	}
	/**
	 *
	 */
	bool Robot::intersects(const Region &aRegion) const
	{
		Region region = getRegion();
		region.Intersect(aRegion);
		return !region.IsEmpty();
	}
	/**
	 *
	 */
	Point Robot::getFrontLeft() const
	{
		// x and y are pointing to top left now
		int x = position.x - (size.x / 2);
		int y = position.y - (size.y / 2);

		Point originalFrontLeft(x, y);
		double angle = Utils::Shape2DUtils::getAngle(front) + 0.5 * Utils::PI;

		Point frontLeft(static_cast<int>((originalFrontLeft.x - position.x) * std::cos(angle) - (originalFrontLeft.y - position.y) * std::sin(angle) + position.x),
						static_cast<int>((originalFrontLeft.y - position.y) * std::cos(angle) + (originalFrontLeft.x - position.x) * std::sin(angle) + position.y));

		return frontLeft;
	}
	/**
	 *
	 */
	Point Robot::getFrontRight() const
	{
		// x and y are pointing to top left now
		int x = position.x - (size.x / 2);
		int y = position.y - (size.y / 2);

		Point originalFrontRight(x + size.x, y);
		double angle = Utils::Shape2DUtils::getAngle(front) + 0.5 * Utils::PI;

		Point frontRight(static_cast<int>((originalFrontRight.x - position.x) * std::cos(angle) - (originalFrontRight.y - position.y) * std::sin(angle) + position.x),
						 static_cast<int>((originalFrontRight.y - position.y) * std::cos(angle) + (originalFrontRight.x - position.x) * std::sin(angle) + position.y));

		return frontRight;
	}
	/**
	 *
	 */
	Point Robot::getBackLeft() const
	{
		// x and y are pointing to top left now
		int x = position.x - (size.x / 2);
		int y = position.y - (size.y / 2);

		Point originalBackLeft(x, y + size.y);

		double angle = Utils::Shape2DUtils::getAngle(front) + 0.5 * Utils::PI;

		Point backLeft(static_cast<int>((originalBackLeft.x - position.x) * std::cos(angle) - (originalBackLeft.y - position.y) * std::sin(angle) + position.x),
					   static_cast<int>((originalBackLeft.y - position.y) * std::cos(angle) + (originalBackLeft.x - position.x) * std::sin(angle) + position.y));

		return backLeft;
	}
	/**
	 *
	 */
	Point Robot::getBackRight() const
	{
		// x and y are pointing to top left now
		int x = position.x - (size.x / 2);
		int y = position.y - (size.y / 2);

		Point originalBackRight(x + size.x, y + size.y);

		double angle = Utils::Shape2DUtils::getAngle(front) + 0.5 * Utils::PI;

		Point backRight(static_cast<int>((originalBackRight.x - position.x) * std::cos(angle) - (originalBackRight.y - position.y) * std::sin(angle) + position.x),
						static_cast<int>((originalBackRight.y - position.y) * std::cos(angle) + (originalBackRight.x - position.x) * std::sin(angle) + position.y));

		return backRight;
	}
	/**
	 *
	 */
	void Robot::handleNotification()
	{
		//	std::unique_lock<std::recursive_mutex> lock(robotMutex);

		static int update = 0;
		if ((++update % 200) == 0)
		{
			notifyObservers();
		}
	}
	/**
	 *
	 */
	void Robot::handleRequest(Messaging::Message &aMessage)
	{
		FUNCTRACE_TEXT_DEVELOP(aMessage.asString());

		switch (aMessage.getMessageType())
		{
		case Messaging::StopCommunicatingRequest:
		{
			aMessage.setMessageType(Messaging::StopCommunicatingResponse);
			aMessage.setBody("StopCommunicatingResponse");
			// Handle the request. In the limited context of this works. I am not sure
			// whether this works OK in a real application because the handling is time sensitive,
			// i.e. 2 async timers are involved:
			// see CommunicationService::stopServer and Server::stopHandlingRequests
			Messaging::CommunicationService::getCommunicationService().stopServer(12345, true);

			break;
		}
		case Messaging::EchoRequest:
		{
			aMessage.setMessageType(Messaging::EchoResponse);
			aMessage.setBody("Messaging::EchoResponse: " + aMessage.asString());
			break;
		}
		default:
		{
			TRACE_DEVELOP(__PRETTY_FUNCTION__ + std::string(": default not implemented"));
			break;
		}
		}
	}
	/**
	 *
	 */
	void Robot::handleResponse(const Messaging::Message &aMessage)
	{
		FUNCTRACE_TEXT_DEVELOP(aMessage.asString());

		switch (aMessage.getMessageType())
		{
		case Messaging::StopCommunicatingResponse:
		{
			//Messaging::CommunicationService::getCommunicationService().stop();
			break;
		}
		case Messaging::EchoResponse:
		{
			break;
		}
		default:
		{
			TRACE_DEVELOP(__PRETTY_FUNCTION__ + std::string(": default not implemented, ") + aMessage.asString());
			break;
		}
		}
	}
	/**
	 *
	 */
	std::string Robot::asString() const
	{
		std::ostringstream os;

		os << "Robot " << name << " at (" << position.x << "," << position.y << ")";

		return os.str();
	}
	/**
	 *
	 */
	std::string Robot::asDebugString() const
	{
		std::ostringstream os;

		os << "Robot:\n";
		os << AbstractAgent::asDebugString();
		os << "Robot " << name << " at (" << position.x << "," << position.y << ")\n";

		return os.str();
	}
	/**
	 *
	 */
	void Robot::drive()
	{
		try
		{
			handleNotificationsFor(Controller::getController());
			World::getWorld().initSensors();
			Controller::getController().initializeFilter();
			Controller::getController().calcPath();

			path = Controller::getController().getPath();

			goal = RobotWorld::getRobotWorld().getGoal("Goal");
		

			unsigned pathPoint = 0;

			if (speed == 0.0)
			{
				speed = 3.5;
			}

			while (!Controller::getController().hasArrived() && driving)
			{
				Controller::getController().chooseAndDoActionFor(ActuatorType::DRIVE);
				Controller::getController().updateBelief();
				std::this_thread::sleep_for(std::chrono::milliseconds(50));
				notifyObservers();

				path = Controller::getController().getPath();

				const PathAlgorithm::Vertex &vertex = path[pathPoint += static_cast<int>(speed)];
				front = BoundedVector(vertex.asPoint(), position);

				position.x = Controller::getController().getKalmanPosition().x;
				position.y = Controller::getController().getKalmanPosition().y;

				// position.x = vertex.x;
				// position.y = vertex.y;

				// setPosition(position);

				if (arrived(goal) || collision())
				{
					Application::Logger::log(
						__PRETTY_FUNCTION__ + std::string(": arrived or collision"));
					notifyObservers();
					break;
				}

			} // while

			speed = 0;

			stopHandlingNotificationsFor(Controller::getController());
		}
		catch (std::exception &e)
		{
			Application::Logger::log(__PRETTY_FUNCTION__ + std::string(": ") + e.what());
			std::cerr << __PRETTY_FUNCTION__ << ": " << e.what() << std::endl;
		}
		catch (...)
		{
			Application::Logger::log(__PRETTY_FUNCTION__ + std::string(": unknown exception"));
			std::cerr << __PRETTY_FUNCTION__ << ": unknown exception" << std::endl;
		}
	}
	/**
	 *
	 */
	void Robot::calculateRoute(GoalPtr aGoal)
	{
		path.clear();
		if (aGoal)
		{
			Application::Logger::setDisable();

			front = BoundedVector(aGoal->getPosition(), position);
			handleNotificationsFor(astar);
			path = astar.search(position, aGoal->getPosition(), size);
			stopHandlingNotificationsFor(astar);

			Application::Logger::setDisable(false);
		}
	}
	/**
	 *
	 */
	bool Robot::arrived(GoalPtr aGoal)
	{
		if (aGoal && intersects(aGoal->getRegion()))
		{
			return true;
		}
		return false;
	}
	/**
	 *
	 */
	bool Robot::collision()
	{
		Point frontLeft = getFrontLeft();
		Point frontRight = getFrontRight();
		Point backLeft = getBackLeft();
		Point backRight = getBackRight();

		const std::vector<WallPtr> &walls = RobotWorld::getRobotWorld().getWalls();
		for (WallPtr wall : walls)
		{
			if (Utils::Shape2DUtils::intersect(frontLeft, frontRight, wall->getPoint1(), wall->getPoint2()) ||
				Utils::Shape2DUtils::intersect(frontLeft, backLeft, wall->getPoint1(), wall->getPoint2()) ||
				Utils::Shape2DUtils::intersect(frontRight, backRight, wall->getPoint1(), wall->getPoint2()))
			{
				return true;
			}
		}
		const std::vector<RobotPtr> &robots = RobotWorld::getRobotWorld().getRobots();
		for (RobotPtr robot : robots)
		{
			if (getObjectId() == robot->getObjectId())
			{
				continue;
			}
			if (intersects(robot->getRegion()))
			{
				return true;
			}
		}
		return false;
	}

} // namespace Model

Controller::Controller(/* args */) : kalmanFilter(nullptr),
									 particleFilter(nullptr), driven(std::make_shared<Model::NormalLine>("GREEN"))
{
}

Controller::~Controller()
{
}

Controller &Controller::getController()
{
	static Controller self;
	return self;
}

bool Controller::hasArrived()
{
	if (kalmanFilter == nullptr && particleFilter == nullptr)
	{
		throw(std::invalid_argument("no method for filtering is selected"));
	}

	auto goal = Model::RobotWorld::getRobotWorld().getGoal("Goal");
	Size goalSize = goal->getSize();
	Point goalPos = goal->getPosition();
	if (goal != nullptr)
	{
		Point position;
		if (kalmanFilter != nullptr)
		{
			position = kalmanData->beliefToPoint();
		}
		else if (particleFilter != nullptr)
		{
			position = particleFilter->posiblePos(particleData->particleDist);
		}
		if (position.y < goalPos.y + goalSize.GetHeight() / 2 && position.y > goalPos.y - goalSize.GetHeight() / 2 && position.x < goalPos.x + goalSize.GetWidth() / 2 && position.x > goalPos.x - goalSize.GetWidth() / 2)
		{
			Application::Logger::log("Goal reached");
			return true;
		}
		if (posOnPath == path.size() - 1)
		{
			Application::Logger::log("End of path");
			return true;
		}
		return false;
	}
	throw(std::invalid_argument("There is no goal"));
}

void Controller::setPath(PathAlgorithm::Path &aPath)
{
	path = aPath;
}

void Controller::calcPath()
{
	if (kalmanFilter == nullptr && particleFilter == nullptr)
	{
		throw(std::invalid_argument("no method for filtering is selected"));
	}

	auto robot = Model::RobotWorld::getRobotWorld().getRobot("Robot");
	auto goal = Model::RobotWorld::getRobotWorld().getGoal("Goal");
	PathAlgorithm::AStar astar;
	if (kalmanFilter != nullptr && goal != nullptr)
	{
		Application::Logger::log("Calculating path for location...");
		handleNotificationsFor(astar);
		Point position = kalmanData->beliefToPoint();
		path = astar.search(position, goal->getPosition(), robot->getSize());
		stopHandlingNotificationsFor(astar);
		Application::Logger::log("Path calculated");
	}
	else if (particleFilter != nullptr && goal != nullptr)
	{
		Application::Logger::log("Calculating new path for location...");
		handleNotificationsFor(astar);
		Point position = particleFilter->posiblePos(particleData->particleDist);
		path = astar.search(position, goal->getPosition(), robot->getSize());
		stopHandlingNotificationsFor(astar);
		Application::Logger::log("New path calculated");
	}
}

void Controller::useKalman()
{
	particleFilter = nullptr;
	particleData = nullptr;
	kalmanData = std::make_unique<KalmanData>();

	if (kalmanData != nullptr)
	{
		kalmanData->A = kalmanData->A.identity();
		kalmanData->B = {
			{0, 0},
			{0, 0}};
		kalmanData->C = kalmanData->C.identity();
		kalmanFilter = std::make_unique<Kalman>(
			kalmanData->A,
			kalmanData->B,
			kalmanData->C,
			kalmanData->procesNoise,
			kalmanData->measurementNoise);
		kalmanData->procesNoise = {
			2,
			2};
		kalmanData->measurementNoise = {
			1,
			1};
	}
}

void Controller::useParticleFilter()
{
	kalmanFilter = nullptr;
	kalmanData = nullptr;
	particleData = std::make_unique<ParticleData>();

	if (particleData != nullptr)
	{
		particleFilter = std::make_unique<ParticleFilter2>();
		particleData->actionOnParticleFunc = [this](Particle &particle)
		{
			static std::shared_ptr<Move> actuator = std::dynamic_pointer_cast<Move>(World::getWorld().getActuator(ActuatorType::DRIVE));
			particle.position = actuator->moveToPos(particle.position, this->action->orientation, this->action->distance);
		};
		particleData->particleDist = particleFilter->initializeBelief();
	}
}

void Controller::chooseAndDoActionFor(ActuatorType actType)
{
	if (kalmanFilter == nullptr && particleFilter == nullptr)
	{
		throw(std::invalid_argument("no filter selected"));
	}
	double radians = 0;
	long long nSteps = (posOnPath + STEP_SIZE > path.size() - 1) ? path.size() - 1 - posOnPath : STEP_SIZE;
	Point position;
	if (kalmanFilter != nullptr)
	{
		position = kalmanData->beliefToPoint();
	}
	else if (particleFilter != nullptr)
	{

		position = particleFilter->posiblePos(particleData->particleDist);
	}

	if (abs(position.x - path[posOnPath].x) < 12 && abs(position.y - path[posOnPath].y) < 12)
	{
		posOnPath += nSteps;
	}

	if (position.x - path[posOnPath].x == 0)
	{
		radians = (position.y - path[posOnPath].y < 0) ? -M_PI / 2 : M_PI / 2;
	}
	else if (position.y - path[posOnPath].y == 0)
	{
		radians = (position.x - path[posOnPath].x < 0) ? M_PI : 0;
	}
	else
	{
		double tempRad = atan((position.y - path[posOnPath].y) / (position.x - path[posOnPath].x));
		if (path[posOnPath].x - position.x < 0)
		{
			radians = (tempRad < 0) ? M_PI - tempRad : -M_PI + tempRad;
		}
		else
		{
			radians = tempRad;
		}
	}
	action = std::make_shared<driveCommands>(radians, nSteps);
	World::getWorld().actuate(actType, std::dynamic_pointer_cast<BaseCommand>(action));
}

void Controller::updateBelief()
{
	if (kalmanFilter == nullptr && particleFilter == nullptr)
	{
		throw(std::invalid_argument("no filter selected"));
	}
	if (kalmanFilter != nullptr)
	{
		Point position = kalmanData->beliefToPoint();

		kalmanPosition = position;

		driven->addPoint(position);

		Kalman::actionsMatrix actions{
			action->distance,
			action->orientation * 180 / M_PI};

		auto odoMeasure = std::dynamic_pointer_cast<OdometerMeasurement>(World::getWorld().measureSensor(SensorType::ODOMETERSENSOR));
		auto compasMeasure = std::dynamic_pointer_cast<CompasMeasurement>(World::getWorld().measureSensor(SensorType::COMPASSENSOR));
		Point measuredPoint(
			static_cast<int>(round(position.x + odoMeasure->distance * cos(compasMeasure->orientation * M_PI / 180))),
			static_cast<int>(round(position.y + odoMeasure->distance * sin(compasMeasure->orientation * M_PI / 180))));

		Kalman::measurementMatrix measurements{
			static_cast<double>(measuredPoint.x),
			static_cast<double>(measuredPoint.y)};

		kalmanData->B = {
			{cos(action->orientation), 0},
			{sin(action->orientation), 0}};

		kalmanFilter->filter(kalmanData->belief, kalmanData->variance, actions, measurements);
	}
	else if (particleFilter != nullptr)
	{

		driven->addPoint(particleFilter->posiblePos(particleData->particleDist));

		ParticleFilter2::measurementsPtrList measurements{
			World::getWorld().measureSensor(SensorType::COMPASSENSOR),
			World::getWorld().measureSensor(SensorType::LIDARSENSOR)};
		particleFilter->filter(particleData->particleDist, particleData->actionOnParticleFunc, measurements);
	}
	notifyObservers();
}

void Controller::initializeFilter()
{
	if (kalmanFilter == nullptr && particleFilter == nullptr)
	{
		throw(std::invalid_argument("no filter selected"));
	}
	World::getWorld().clearLines();
	driven->clear();

	if (kalmanFilter != nullptr && kalmanData != nullptr)
	{
		Point pos = World::getWorld().getPosRobot();
		action = std::make_shared<driveCommands>(0, 0);
		kalmanData->belief[KalmanData::BeliefValue::X][0] = pos.x;
		kalmanData->belief[KalmanData::BeliefValue::Y][0] = pos.y;

		kalmanData->variance[KalmanData::BeliefValue::X][KalmanData::BeliefValue::X] = 10;
		kalmanData->variance[KalmanData::BeliefValue::Y][KalmanData::BeliefValue::Y] = 10;
	}
	else if (particleFilter != nullptr)
	{
		action = std::make_shared<driveCommands>(M_PI / 4, STEP_SIZE);

		std::size_t trie = 0;

		Point prevPoint(1024, 1024);
		Point currentPoint(0, 0);
		double distance = 100;

		while (distance > 20)
		{
			if (trie > 50)
			{
				throw(std::logic_error("unable to find position"));
			}

			World::getWorld().actuate(ActuatorType::DRIVE, std::dynamic_pointer_cast<BaseCommand>(action));

			ParticleFilter2::measurementsPtrList measurements{
				World::getWorld().measureSensor(SensorType::COMPASSENSOR),
				World::getWorld().measureSensor(SensorType::LIDARSENSOR)};

			currentPoint = particleFilter->posiblePos(particleData->particleDist);

			particleFilter->filter(particleData->particleDist, particleData->actionOnParticleFunc, measurements);

			driven->addPoint(currentPoint);

			++trie;

			distance = sqrt(pow(currentPoint.x - prevPoint.x, 2) + pow(currentPoint.y - prevPoint.y, 2));
			prevPoint = currentPoint;

			notifyObservers();
		}
		driven->clear();
	}
}

std::vector<Model::LinePtr> Controller::getLines()
{
	std::vector<Model::LinePtr> lines{driven};
	return lines;
}

void Controller::handleNotification()
{
	static int update = 0;
	if ((++update % 200) == 0)
	{
		notifyObservers();
	}
}
