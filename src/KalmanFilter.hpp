#ifndef ROBOTWORLDWOR_KALMANFILTER_HPP
#define ROBOTWORLDWOR_KALMANFILTER_HPP

#include "Matrix.hpp"

template<std::size_t n_beliefs, std::size_t n_measurements, std::size_t n_actions>
class KalmanFilter
{
public:
    typedef Matrix<double, n_beliefs, 1> beliefMatrix;
    typedef Matrix<double, n_beliefs, n_beliefs> varianceMatrix;
    typedef Matrix<double, n_actions, 1> actionsMatrix;
    typedef Matrix<double, n_measurements, 1> measurementMatrix;

    typedef Matrix<double, n_beliefs, n_beliefs> matrixA;
    typedef Matrix<double, n_beliefs, n_actions> matrixB;
    typedef Matrix<double, n_measurements, n_beliefs> matrixC;

    typedef Matrix<double, n_beliefs, 1> procesNoiseMatrix;
    typedef Matrix<double, n_measurements, 1> measureNoiseMatrix;

    KalmanFilter(matrixA& A, matrixB& B, matrixC& C,
                 procesNoiseMatrix& procesNoise,
                 measureNoiseMatrix& measureNoise);

    ~KalmanFilter();

    void filter(
            beliefMatrix& belief,
            varianceMatrix& variance,
            const actionsMatrix& actions,
            const measurementMatrix& measeurements
    );

private:
    matrixA& A;
    matrixB& B;
    matrixC& C;
    procesNoiseMatrix& procesNoise;
    measureNoiseMatrix& measureNoise;
};

template<std::size_t n_beliefs, std::size_t n_measurements, std::size_t n_actions>
void KalmanFilter<n_beliefs, n_measurements, n_actions>::filter(
        beliefMatrix& belief, varianceMatrix& variance,
        const actionsMatrix& actions, const measurementMatrix& measeurements)
{
    Matrix<double, n_beliefs, 1> _u = A*belief+B*actions;

    Matrix<double, n_beliefs, n_beliefs> _E = A*variance*A.transpose() + procesNoise*procesNoise.transpose();

    auto S = C*_E*C.transpose() + (measureNoise*measureNoise.transpose());

    Matrix<double, n_beliefs, n_measurements> K = _E*C.transpose()*S.inverse();

    belief = _u + K*(measeurements - C*_u);

    variance = (_E.identity() - K*C)*_E;
}

template<std::size_t n_beliefs, std::size_t n_measurements, std::size_t n_actions>
KalmanFilter<n_beliefs, n_measurements, n_actions>::KalmanFilter(matrixA& A, matrixB& B, matrixC& C,
                                                                 procesNoiseMatrix& procesNoise,
                                                                 measureNoiseMatrix& measureNoise) :
        A(A), B(B), C(C), procesNoise(procesNoise), measureNoise(measureNoise)
{
}

template<std::size_t n_beliefs, std::size_t n_measurements, std::size_t n_actions>
KalmanFilter<n_beliefs, n_measurements, n_actions>::~KalmanFilter()
{
}

#endif //ROBOTWORLDWOR_KALMANFILTER_HPP
