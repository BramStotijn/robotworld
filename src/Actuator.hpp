#ifndef ROBOTWORLDWOR_ACTUATOR_HPP
#define ROBOTWORLDWOR_ACTUATOR_HPP


#include "Point.hpp"
#include "RandomDistrib.hpp"

#include <memory>

enum class ActuatorType : unsigned int;

class World;

struct BaseCommand
{
    BaseCommand();
    virtual ~BaseCommand();
};

class Actuator
{
public:
    Actuator();
    virtual ~Actuator();
    virtual void handleCommand(std::shared_ptr<BaseCommand> anCommand) = 0;
    ActuatorType getType();
protected:
    ActuatorType type;
};

#endif //ROBOTWORLDWOR_ACTUATOR_HPP
