#ifndef ROBOTWORLDWOR_RANDOMDISTRIB_HPP
#define ROBOTWORLDWOR_RANDOMDISTRIB_HPP

#include <random>

template<typename dist, typename distType>
class RandomDistribution
{
private:
    static_assert( std::is_arithmetic<distType>::value, "Value distType must be arithmetic, see http://en.cppreference.com/w/cpp/types/is_arithmetic");
    static std::random_device rd;
    static std::mt19937 generator;
    dist distrib;
public:
    RandomDistribution(distType distValA, distType distValB);
    virtual ~RandomDistribution();
    distType operator()();
};

template<typename dist, typename distType>
std::random_device RandomDistribution<dist, distType>::rd{};

template<typename dist, typename distType>
std::mt19937 RandomDistribution<dist, distType>::generator{RandomDistribution<dist, distType>::rd()};

template<typename dist, typename distType>
RandomDistribution<dist, distType>::RandomDistribution(distType distValA, distType distValB)
{
    distrib = dist{distValA, distValB};
}

template<typename dist, typename distType>
RandomDistribution<dist, distType>::~RandomDistribution()
{
}

template<typename dist, typename distType>
distType RandomDistribution<dist, distType>::operator()()
{
    return distrib(generator);
}

#endif //ROBOTWORLDWOR_RANDOMDISTRIB_HPP
