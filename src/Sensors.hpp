#ifndef ROBOTWORLDWOR_SENSORS_HPP
#define ROBOTWORLDWOR_SENSORS_HPP

#include <memory>
#include <array>
#include "RandomDistrib.hpp"
#include "Point.hpp"
#include "Wall.hpp"
#include "Sensor.hpp"

enum class SensorType : unsigned int
{
    LIDARSENSOR,
    ODOMETERSENSOR,
    COMPASSENSOR
};

struct LidarMeasurement : public Measurement
{
    LidarMeasurement();
    virtual ~LidarMeasurement();
    std::array<double, 180> distances;
    virtual double ParticleWeight(const Particle& particle) const;
};

class LidarSensor : public Sensor
{
public:
    double slopes[90];
    std::array<Model::WallPtr, 4> wallsInWorld;
    LidarSensor();
    LidarSensor(double noiseMean, double noiseDist);
    virtual void init();
    virtual ~LidarSensor();
    std::shared_ptr<Measurement> measure(const World& world);
    std::array<double, 180> wallDistance(const Point& from, std::vector<Model::WallPtr> walls);
};

struct CompasMeasurement : public Measurement
{
    CompasMeasurement();
    virtual ~CompasMeasurement();
    double orientation;
    virtual double ParticleWeight(const Particle& particle) const;
};

class CompasSensor : public Sensor
{
public:
    CompasSensor();
    CompasSensor(double noiseMean, double noiseDist);
    virtual ~CompasSensor();
    std::shared_ptr<Measurement> measure(const World& world);
    virtual void init();
};

struct OdometerMeasurement : public Measurement
{
    OdometerMeasurement();
    virtual ~OdometerMeasurement();
    double distance;
    virtual double ParticleWeight(const Particle& particle) const;
};

class OdometerSensor : public Sensor
{
public:
    OdometerSensor();
    OdometerSensor(double noiseMean, double noiseDist);
    virtual ~OdometerSensor();
    std::shared_ptr<Measurement> measure(const World& world);
    virtual void init();
private:
    Point prevPos;
    double driveDistance;
};

#endif //ROBOTWORLDWOR_SENSORS_HPP
