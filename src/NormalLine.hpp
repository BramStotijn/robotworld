#ifndef ROBOTWORLDWOR_NORMALLINE_HPP
#define ROBOTWORLDWOR_NORMALLINE_HPP

#include "Config.hpp"
#include "ModelObject.hpp"
#include "Point.hpp"
#include <climits>

namespace Model
{
    class NormalLine;
    typedef std::shared_ptr<NormalLine> LinePtr;

    class NormalLine :  public ModelObject
    {
    public:
        /**
         *
         */
        NormalLine(  const std::string& colour = "BLACK");
        /**
         *
         */
        NormalLine(	const Point& aPoint1,
                 const Point& aPoint2,
                 const std::string& colour = "BLACK");
        /**
         *
         */
        virtual ~NormalLine();
        /**
         *
         */
        void setConnected(bool connected);

        bool isConnected();

        void clear();
        const std::vector<Point>& getPoints();

        const std::string& getColour();

        Point getPointAt(std::size_t index) const;
        /**
         *
         */
        void setPointAt( const Point& aPoint, std::size_t index,
                         bool aNotifyObservers = true);

        void addPoint(const Point& aPoint, bool aNotifyObservers = true);

    private:
        bool connect;
        std::string colour;
        std::vector<Point> points;
    };
} // namespace Model

#endif //ROBOTWORLDWOR_NORMALLINE_HPP
